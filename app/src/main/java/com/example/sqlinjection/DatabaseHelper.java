package com.example.sqlinjection;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Linda Vu on 9/19/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String TB_NAME = "usertable";
    public static final String ID = "_id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private void insertUser(SQLiteDatabase db, String username, String password) {
        db.execSQL("INSERT INTO " + TB_NAME + "(" + USERNAME + ", " + PASSWORD + ") VALUES (?, ?)", new String[]{ username, password });
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create user table
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_NAME + "(" + ID + " INTEGER PRIMARY KEY, " +
                    USERNAME + " VARCHAR, " + PASSWORD + " VARCHAR)");
        insertUser(db, "admin", "admin888");
        insertUser(db, "root", "root123");
        insertUser(db, "wanqing", "wanqing");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

}
