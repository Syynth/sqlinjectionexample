package com.example.sqlinjection;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;

public class MainActivity extends AppCompatActivity {

    public static final String DB_NAME = "sqlinjectionexample";
    public static final int DB_VERSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button button = (Button) findViewById(R.id.button);
        final MainActivity self = this;
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((Switch) self.findViewById(R.id.switch1)).isChecked()) {
                    self.handleSearchSafely();
                } else {
                    self.handleSearchDangerously();
                }

            }
        });
    }

    public void handleSearchDangerously() {
        try {
            final String id = ((EditText) findViewById(R.id.editText)).getText().toString();
            final SQLiteDatabase db = new DatabaseHelper(this, DB_NAME, null, DB_VERSION).getWritableDatabase();
            final Cursor cursor = db.rawQuery("SELECT * FROM " + DatabaseHelper.TB_NAME + " WHERE " +
                    DatabaseHelper.ID + "=" + id, null);
            displaySearchResults(cursor);
        } catch (Exception ex) {
            ((TextView) findViewById(R.id.textView)).setText("Error occured\r\n" + ex.toString());
        }

    }

    public void handleSearchSafely() {
        final String id = ((EditText) findViewById(R.id.editText)).getText().toString();
        final SQLiteDatabase db = new DatabaseHelper(this, DB_NAME, null, DB_VERSION).getWritableDatabase();
        final Cursor cursor = db.rawQuery("SELECT * FROM " + DatabaseHelper.TB_NAME + " WHERE " +
                DatabaseHelper.ID + "=?", new String[]{id});
        displaySearchResults(cursor);
    }

    private void displaySearchResults(Cursor cursor) {
        String result = "";
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            result += "id: " + cursor.getInt(0) + "\r\n";
            result += "user: " + cursor.getString(1) + "\r\n";
            result += "password: " + cursor.getString(2) + "\r\n\r\n";
            cursor.moveToNext();
        }
        cursor.close();
        if ("".equals(result)) {
            result = "No results found";
        }
        ((TextView) findViewById(R.id.textView)).setText(result);
    }
}
